# MAPARE

<br>

### Introduction

A script to automate parsing manjaro package lists and some related utilities.

Supports the official and community edition desktops: GNOME, KDE, XFCE, i3, & Cinnamon.

<br>

### Manual
```text
 Manjaro Package Restore

 Retrieve and reinstall package lists

 Options:

   -h to show this help

   -I to install missing packages (--needed by default)

 Install options (require -I):

   -B to limit package list, unless combined with another limit, to base root profile
   -D to limit package list, unless combined with another limit, to desktop profile
   -A to not omit already installed packages from any transaction
   -E to mark packages to be installed with mapare as explicitly installed
   -R to ignore package lists and only reinstall native system packages
   -P to ignore install instructions and only print the package list

   Use the additional --overwrite flag to overwrite existing files
   (Exercise Caution)

 Non-default packages:

   -X to show any packages not included with a default installation

 Keyring Assistant:

   -K to begin keyring assistant prompts

```

<br>

### Run the script remotely via the URL

The syntax looks like this:<br>
`bash <(curl -s https://gitlab.com/cscs/mapare/-/raw/main/mapare) {options}`

##### Example run printing what would be installed:
```text
bash <(curl -s https://gitlab.com/cscs/mapare/-/raw/main/mapare) -IP
```

##### Example run launching the Key Assistant:
```text
bash <(curl -s https://gitlab.com/cscs/mapare/-/raw/main/mapare) -K
```

<br>

### Download and use

##### Download:
```text
curl -O https://gitlab.com/cscs/mapare/-/raw/main/mapare
```

##### Mark Executable:

```text
chmod +x mapare
```

##### Run:

```text
./mapare -I
```

##### Export output:

```text
./mapare -X | tee mapareout.txt
```

<br></br>

### Donate

Everything is free, but you can donate using these:  

[<img src="https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2" width=160px>](https://ko-fi.com/X8X0VXZU) &nbsp;[<img src="https://gitlab.com/cscs/resources/raw/master/paypalkofi.png" width=160px />](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52)

<br>
