#!/usr/bin/env bash
#               (Ma)njaro (Pa)ckage (Re)store                #
#                           mapare                           #
##    Script to parse and install package lists and more    ##
##                            <3                            ##
#####                        cscs                        #####

# If no argument print help; if help invoked at all it takes precedence
[[ $# = 0 || $* = *[Hh]* ]] && set -- -h

# Set the locale based on the environment
export LANG="${LANG:-en_US.UTF-8}"
export LC_ALL="${LC_ALL:-en_US.UTF-8}"

# Use gettext to translate messages
gettext_wrapper() {
    local message="$1"
    local translated_message=$(gettext "$message")
    if [ -z "$translated_message" ]; then
        echo "$message"
    else
        echo "$translated_message"
    fi
}

# Header
echo
echo " Manjaro Package Restore"
echo
echo " Retrieve and reinstall package lists"
echo

# The Help
_mapare_help() {
_ma_help_trans=$(gettext \
" Options:

   -h to show this help

   -I to install missing packages (--needed by default)

 Install options (require -I):

   -B to limit package list, unless combined with another limit, to base root profile
   -D to limit package list, unless combined with another limit, to desktop profile
   -A to not omit already installed packages from any transaction
   -E to mark packages to be installed with mapare as explicitly installed
   -R to ignore package lists and only reinstall native system packages
   -P to ignore install instructions and only print the package list

   Use the additional --overwrite flag to overwrite existing files
   (Exercise Caution)

 Non-default packages:

   -X to show any packages not included with a default installation

 Keyring Assistant:

   -K to begin keyring assistant prompts
")
echo -ne "$_ma_help_trans"'\n\n'
}

# Get desktop and desktop profile path
_mapare_getdesk() {
    while true; do
        read -rp " Please provide desktop environment (kde,xfce,gnome): "  _ma_deskanswer
        case $_ma_deskanswer in
            [Cc][Ii][Nn][Nn][Aa][Mm][Oo][Nn] ) break ;;
            [Gg][Nn][Oo][Mm][Ee] ) break ;;
            [Ii][3] ) break ;;
            [Kk][Dd][Ee] ) break ;;
            [Xx][Ff][Cc][Ee] ) break ;;
            * ) echo -e "\n Please input a supported desktop.\n\n Choices include 'Cinnamon', 'Gnome', 'i3', 'KDE', and 'XFCE'\n" ;;
        esac
    done
    # Iterates lowercase
    _ma_deskchoice=${_ma_deskanswer,,}
    # We need to know if its a community edition for the right URL
    if [[ "$_ma_deskchoice" = @(cinnamon|i3) ]]; then
        _mrppath=community
    else
        _mrppath=manjaro
    fi
}

# Get all profiles for sure
_mapare_max_payload() {
    if [[ ${#_mrpsrcs[@]} != "$_mrpsrc1" ]]; then
        _mrpsrc1=https://gitlab.manjaro.org/profiles-and-settings/iso-profiles/-/raw/master/shared/Packages-Root
    fi
    if [[ ${#_mrpsrcs[@]} != "$_mrpsrc2" ]]; then
        _mapare_getdesk
        _mrpsrc2=https://gitlab.manjaro.org/profiles-and-settings/iso-profiles/-/raw/master/"$_mrppath"/"$_ma_deskchoice"/Packages-Desktop
    fi
    if [[ ${#_mrpsrcs[@]} != "$_mrpsrc3" ]]; then
        _mrpsrc3=https://gitlab.manjaro.org/profiles-and-settings/iso-profiles/-/raw/master/shared/Packages-Mhwd
    fi
    _mrpsrcs=("$_mrpsrc1" "$_mrpsrc2" "$_mrpsrc3")
}

# Get all payloads clean
_mapare_cln_payload() {
    _ma_lclkernz=$(pacman -Qqs "^linux[0-9][0-9]?([0-9])$")
    rm -rf /tmp/mapare_pkglists >/dev/null 2>&1
    if command -v wget >/dev/null 2>&1; then
        wget -qP /tmp/mapare_pkglists "${_mrpsrcs[@]}"
    else
        for _mrpurl in "${_mrpsrcs[@]}"; do curl -sS --create-dirs --output-dir /tmp/mapare_pkglists -LO "$_mrpurl"; done
    fi
    if [[ -e /tmp/mapare_pkglists/Packages-Root ]]; then
        echo "$_ma_lclkernz" | tee -a /tmp/mapare_pkglists/Packages-Root >/dev/null 2>&1
    fi
    if [[ -e /tmp/mapare_pkglists/Packages-Desktop ]]; then
        sed -i '/^\(#\|>office\)/d;s/^\(>multilib\|>basic\) //' /tmp/mapare_pkglists/Packages-Desktop
    fi
    if [[ -e /tmp/mapare_pkglists/Packages-Mhwd ]]; then
        for _ma_linz in $_ma_lclkernz; do
            sed -i '/KERNEL/{p;s/KERNEL/'"$_ma_linz"'/;}' /tmp/mapare_pkglists/Packages-Mhwd
        done
        sed -i '/^\(#\|>nonfree\|>office\)/d;s/^>multilib //' /tmp/mapare_pkglists/Packages-Mhwd
    fi
    _ma_pkgpayload=$(awk '$1 ~ /^[a-z0-9]/ {print $1}' /tmp/mapare_pkglists/*)
}

# The regular print function
_mapare_print() {
    echo -e "\n Packages for installation:\n"
    pacman -Sp $_ma_pkgpayload $_ma_need --print-format "%n %v"
}

# The Keyring Assistant initiates steps to ameliorate common problems related to keys
_mapare_keyfix() {
    ## Announce
    echo -e " $(gettext "Keyring Assistant")"
    ## Clear sudo creds and ask again
    sudo -k
    echo
    sudo -v || exit
    echo
    ## Localize the affirmative
    _ma_yexp=$(locale yesexpr)
    _ma_yas=$(locale yesstr);
    _ma_naspre=$(locale nostr);
    _ma_nas=${_ma_naspre::1};
    _ma_yn=$(echo " ("${_ma_yas:0:1}"/"${_ma_nas^^}")? ")

    ## Cache
    echo -ne "\n$(gettext "Clearing the package cache may be useful to remove corrupted packages.")\n"

    read -rp $'\n'"$(gettext "Remove unused repositories and ALL files from cache")$_ma_yn"  _ma_krsp
    if [[ "$_ma_krsp" =~ $_ma_yexp ]]; then
        yes | LC_ALL=C sudo pacman -Scc --color=never >/dev/null 2>&1
        echo -e "$(gettext "Done")"
    else
        echo -e "$(gettext "Skipped")"
    fi

    ## Remove current keys
    read -rp $'\n'"$(gettext "Remove local pacman keys")$_ma_yn"  _ma_krsp
    if [[ "$_ma_krsp" =~ $_ma_yexp ]]; then
        sudo rm -r /etc/pacman.d/gnupg
        echo -e "$(gettext "Done")"
    else
        echo -e "$(gettext "Skipped")"
    fi

    ## Download packages
    read -rp $'\n'"$(gettext "Download current keyring packages")$_ma_yn"  _ma_krsp
    if [[ $_ma_krsp =~ $_ma_yexp  ]]; then
    ## Silently find current versions
    _manj_keysv=$(LC_ALL=C pacman -Si manjaro-keyring | awk '/^Version/ {print $3}')
    _arch_keysv=$(LC_ALL=C pacman -Si archlinux-keyring | awk '/^Version/ {print $3}')
    sudo rm -r /tmp/mapare_keys >/dev/null 2>&1
        if command -v wget >/dev/null 2>&1; then
            wget -qP /tmp/mapare_keys https://mirror.easyname.at/manjaro/pool/overlay/manjaro-keyring-"$_manj_keysv"-any.pkg.tar.zst
            wget -qP /tmp/mapare_keys https://mirror.easyname.at/manjaro/pool/sync/archlinux-keyring-"$_arch_keysv"-any.pkg.tar.zst
        else
            curl --create-dirs --output-dir /tmp/mapare_keys -O https://mirror.easyname.at/manjaro/pool/overlay/manjaro-keyring-"$_manj_keysv"-any.pkg.tar.zst
            curl --create-dirs --output-dir /tmp/mapare_keys -O https://mirror.easyname.at/manjaro/pool/sync/archlinux-keyring-"$_arch_keysv"-any.pkg.tar.zst
        fi
        echo -e "$(gettext "Done")"
    else
        echo -e "$(gettext "Skipped")"
    fi

    ## Install keyring packages
    read -rp $'\n'"$(gettext "Install downloaded keyring packages")$_ma_yn"  _ma_krsp
    if [[ $_ma_krsp =~ $_ma_yexp ]]; then
        sudo pacman -U /tmp/mapare_keys/*-keyring-*-any.pkg.tar.zst
        echo -e "$(gettext "Done")"
    else
        echo -e "$(gettext "Skipped")"
    fi

    ## Init and populate keyring
    read -rp $'\n'"$(gettext "Initialize and populate keys")$_ma_yn"  _ma_krsp
    if [[ $_ma_krsp =~ $_ma_yexp ]]; then
        sudo pacman-key --init
        sudo pacman-key --populate manjaro archlinux
        echo -e "$(gettext "Done")"
    else
        echo -e "$(gettext "Skipped")"
    fi

    ## Reset mirror pool
    read -rp $'\n'"$(gettext "Reset mirror pool")$_ma_yn"  _ma_krsp
    if [[ $_ma_krsp =~ $_ma_yexp ]]; then
        sudo pacman-mirrors -c all
        echo -e "$(gettext "Done")"
    else
        echo -e "$(gettext "Skipped")"
    fi

    ## Sort mirrors, sync and update
    read -rp $'\n'"$(gettext "Sort mirrors, resync, and update")$_ma_yn"  _ma_krsp
    if [[ $_ma_krsp =~ $_ma_yexp ]]; then
        sudo pacman-mirrors -f
        sudo pacman -Syu
        echo -e "$(gettext "Done")"
    else
        echo -e "$(gettext "Skipped")"
    fi

    echo -ne "\n$(gettext "Finished!")\n\n"
}

# By default only iterate packages not installed - flags can override
_ma_need="--needed"

# The main arguments
while getopts 'KXIBDMFAERPHh' _ma_func; do
    case "$_ma_func" in

        K)  _mapare_keyfix; exit ;;

        X)  if command -v pactree >/dev/null 2>&1; then
                rm -r /tmp/mapare_xenos >/dev/null 2>&1
                mkdir -p /tmp/mapare_xenos
                _mapare_max_payload
                # announce and begin progress bar process
                echo -ne "\n Parsing full package lists and dependencies can take some minutes.\n "; while :; do echo -n .; sleep 1; done & _ma_progress_pid=$!
                _mapare_cln_payload
                pacman -Qsq | sort -u | tee /tmp/mapare_xenos/localpkgs >/dev/null 2>&1
                for _mrppaks in $_ma_pkgpayload; do pactree -lus "$_mrppaks"; done | sort -u | tee /tmp/mapare_xenos/defaultpkgs >/dev/null 2>&1
                # silently kill the progress bar background process
                kill $_ma_progress_pid && wait $_ma_progress_pid &> /dev/null
                echo -e "\n\n Packages not matching default package lists:\n"
                comm -13 /tmp/mapare_xenos/defaultpkgs /tmp/mapare_xenos/localpkgs
                echo; exit
            else
                echo -e "\n The pactree command is required for this function.\n\n"; exit
            fi ;;

        I)  _ma_installing=1
            _mrpsrc1=https://gitlab.manjaro.org/profiles-and-settings/iso-profiles/-/raw/master/shared/Packages-Root
            _mrpsrc3=https://gitlab.manjaro.org/profiles-and-settings/iso-profiles/-/raw/master/shared/Packages-Mhwd
            if [[ $* != *M* && $* != *D* && $* != *R* && $* != *F* ]]; then
                if [[ -z $_mrpsrc2 ]]; then
                    _mapare_getdesk
                    _mrpsrc2=https://gitlab.manjaro.org/profiles-and-settings/iso-profiles/-/raw/master/"$_mrppath"/"$_ma_deskchoice"/Packages-Desktop
                    _mrpsrcs+=("$_mrpsrc1" "$_mrpsrc2")
                fi
            fi ;;

        B)  _mrpsrc1=https://gitlab.manjaro.org/profiles-and-settings/iso-profiles/-/raw/master/shared/Packages-Root
            if [[ $* = *D* || $* = *M* ]]; then
                _mrpsrcs+=("$_mrpsrc1")
            else _mrpsrcs=("$_mrpsrc1")
            fi ;;

        D)  _mapare_getdesk
            _mrpsrc2=https://gitlab.manjaro.org/profiles-and-settings/iso-profiles/-/raw/master/"$_mrppath"/"$_ma_deskchoice"/Packages-Desktop
            if [[ $* = *M* || $* = *B* ]]; then
                _mrpsrcs+=("$_mrpsrc2")
            else _mrpsrcs=("$_mrpsrc2")
            fi ;;

        M)  _mrpsrc3=https://gitlab.manjaro.org/profiles-and-settings/iso-profiles/-/raw/master/shared/Packages-Mhwd
            if [[ $* = *D* || $* = *B* ]]; then
                _mrpsrcs+=("$_mrpsrc3")
            else _mrpsrcs=("$_mrpsrc3")
            fi ;;

        F)  _mapare_max_payload ;;

        A)  _ma_need="" ;;

        E)  _ma_explicit="--asexplicit" ;;

        R)  _ma_reinstalling=1
            _ma_need=""
            _ma_pkgpayload=$(pacman -Qnq) ;;

        P)  if (( ${#_mrpsrcs[@]} == 0 )) && [[ -z $_ma_reinstalling ]]; then
                echo -ne "\nNothing to print. See Help. \n\n"
                _mapare_help; exit
            fi
            if [[ -z $_ma_reinstalling ]]; then
                _mapare_cln_payload
            fi
            _mapare_print
            echo; exit ;;

        H|h|?)  _mapare_help; exit ;;

    esac

    shift $(( OPTIND - 1 ))

done

# No sources and not Reinstalling means help and exit
if (( ${#_mrpsrcs[@]} == 0 )) && [[ -z $_ma_reinstalling ]]; then
    echo -ne "\nNothing to do. See Help. \n\n"
    _mapare_help; exit
fi

# If not Reinstalling then make sure to clean payload
if [[ -z $_ma_reinstalling ]]; then
    _mapare_cln_payload
fi

# Final safety - if Install option switch not enabled then print and exit
if [[ -z $_ma_installing ]]; then
    _mapare_print
    echo; exit
fi

# Pacman package transaction; add the semi-dangerous overwrite flag if asked
if [[ $1 == '--overwrite' ]]; then
    sudo pacman -Syu $_ma_pkgpayload $_ma_need $_ma_explicit --overwrite '*'
else
    sudo pacman -Syu $_ma_pkgpayload $_ma_need $_ma_explicit
fi

echo; exit 0
